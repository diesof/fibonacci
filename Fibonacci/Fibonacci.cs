﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Fibonacci
    {
        public void MostrarSerieFibonacci(int cantidadSeries)
        {
            int num1 = 0;
            int num2 = 1;
            int serie = 1;

            for (int i = 1; i <= cantidadSeries; i++)
            {
                if (i == 1)
                {
                    Console.Write(num1);
                }
                else if (i == 2)
                {
                    Console.Write(serie);
                }
                else
                {
                    Console.Write(serie);

                    num1 = num2;
                    num2 = serie;
                    serie = num1 + num2;
                }

                if (i != cantidadSeries)
                {
                    Console.Write("-");
                }
            }

            Console.WriteLine("Esto es una prueba de Diego");

            Console.WriteLine("Esto es la segunda prueba de Diego");
        }
    }
}