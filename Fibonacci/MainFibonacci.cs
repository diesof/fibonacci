﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class MainFibonacci
    {
        static void Main(string[] args)
        {
            Fibonacci f = new Fibonacci();

            Console.Write("Ingrese la longitud de la serie Fibonacci: ");
            int longitudSeries = int.Parse(Console.ReadLine());

            f.MostrarSerieFibonacci(longitudSeries);

            Console.WriteLine("\n\nPresione cualquier tecla para cerrar la aplicación...");

            Console.ReadKey();
        }
    }
}